# Adaptador - Planilha Empresas DNA USP

Este repositório contém funções e utilitários para ler e mapear planilhas de empresas DNA USP, além de realizar validação linha a linha.

## Módulos

### Field validators (`field_validators`)

Concentra validadores de campo (i.e. uma única coluna), os validadores estão em `field_validators/validators`. Cada validador deve receber um valor, e irá devolver um resultado do tipo `ValidationResult`.

- Tipo `ValidationResult`:

  Contém os atributos `isOk` e `value`, se `isOk`, então `value` contém um valor validado e possivelmente modificado. Caso contrário, o `value` é do tipo `RowError`, indicando o erro ocorrido.

- Funções de validação:

  Sempre recebem um valor qualquer e devolvem um `ValidationResult`, não se esqueça de usar o decorador `field_validator` para encapsular possíveis erros não tratados.

  Opcionalmente (mas recomendado), a função recebe um `kwarg` com nome `field`, e a utiliza para indicar o nome do campo caso devolva um `RowError`.

- Presets:

  São apenas dicionários que fazem a equivalência de nomes de colunas para funções de validação, pode ser usado com o módulo `converter` e é recomendado que esteja coeso com algum `mapper`.

### Mapper (`mapper`)

Serve apenas para mapear/renomear DataFrames, os `presets` são configurações comuns para serem utilizadas.

### Validator (`validator`)

Realiza a validação de `DataFrame`s e suas linhas (`Series`) utilizando `field_validators`.

- Função `validate_row`:

  Realiza validação de uma linha do DataFrame, recebe a linha (`Series`) e um dicionário que mapeia nomes de colunas a seu respectivo `field_validator`.

- Função `validate_df`:

  Realiza validação de um DataFrame, recebe o DataFrame e um dicionário que mapeia nomes de colunas a seu respectivo `field_validator`, utiliza a função `validate_row` por baixo dos panos. Também recebe, opcionalmente.

### Common (`/`)

- Tipo `RowError`:
  Representa um erro em uma linha, com os campos `field` (nome do campo), `reason` (motivo), `value` (valor).
