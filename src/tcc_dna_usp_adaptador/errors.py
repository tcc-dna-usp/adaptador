class RowError:
    def __init__(self, field=None, reason=None, value=None) -> None:
        self.field = field
        self.reason = reason
        self.value = value

    def __str__(self) -> str:
        return f"""Campo: {self.field}\tMotivo: {self.reason}"""

    def __repr__(self) -> str:
        return self.__str__()
