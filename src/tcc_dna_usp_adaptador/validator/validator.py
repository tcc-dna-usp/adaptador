from typing import List, Tuple
from pandas.core.frame import DataFrame
from tcc_dna_usp_adaptador.validator.util import RowResult
from tcc_dna_usp_adaptador.validator.util import get_value
import pandas as pd
from tcc_dna_usp_adaptador.field_validators.presets import preset_cnpj


def validate_row(_row: pd.Series, fields=preset_cnpj) -> RowResult:
    row = _row.copy()

    errors = []

    for (key, validate_functions) in fields.items():
        _value_result = get_value(row, key)
        if not _value_result.isOk:
            errors.append(_value_result)
            continue
        _value = _value_result.value

        for validate in validate_functions:
            result = validate(_value, field=key)
            if result.isOk:
                _value = result.value
            else:
                row_err = result.value
                row_err.field = key
                errors.append(result.value)

        row[key] = _value

    return RowResult(row, errors)


def validate_df(_df: pd.DataFrame, fields=preset_cnpj) -> Tuple[DataFrame, List[RowResult]]:
    orig = _df
    df = _df.drop(_df.index)
    errors = []

    for _, value in orig.iterrows():
        result = validate_row(value, fields=fields)
        if len(result.errors) == 0:
            df.loc[len(df)] = result.value
        else:
            errors.append(result)

    return df, errors
