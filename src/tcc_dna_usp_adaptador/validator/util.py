from tcc_dna_usp_adaptador.errors import RowError
import pandas as pd


class GetRowValueResult:
    def __init__(self, isOk: bool, column: str, value=None) -> None:
        self.value = value
        self.column = column
        self.isOk = isOk

    def ok(column: str, value=None):
        return GetRowValueResult(True, column, value)

    def error(column: str, value=None):
        return GetRowValueResult(False, column, value)


class RowResult:
    def __init__(self, value, errors=[]) -> None:
        self.value = value
        self.errors = errors

    def isOk(self) -> bool:
        return len(self.errors) == 0

    def get_index(self) -> str:
        try:
            return str(int(self.value.loc['_index']))
        except:
            return 'desconhecido'

    def __str__(self) -> str:
        index = self.get_index()
        if self.isOk():
            return f"Índice {index} - ok"
        return f"Índice {index} - Erros: {self.errors}"

    def __repr__(self) -> str:
        return str(self)


def get_value(s: pd.Series, key: str):
    try:
        return GetRowValueResult.ok(key, s[key])
    except:
        return GetRowValueResult.error(key, RowError(field=key, reason="Dado faltante"))
