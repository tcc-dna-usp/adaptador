from functools import wraps
from tcc_dna_usp_adaptador.errors import RowError


class ValidationResult:
    def __init__(self, isOk: bool, value) -> None:
        self.isOk = isOk
        self.value = value

    def ok(value):
        return ValidationResult(True, value)

    def error(value, field=None, reason=None):
        return ValidationResult(isOk=False, value=RowError(field, reason, value))


def field_validator(func):
    @wraps(func)
    def wrapped(*args, **kwargs):
        try:
            return func(*args, **kwargs)
        except:
            return ValidationResult.error((args), field=kwargs.get("field"),  reason="Erro desconhecido")

    return wrapped
