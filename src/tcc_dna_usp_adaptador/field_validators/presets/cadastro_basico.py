from tcc_dna_usp_adaptador.field_validators import validators


preset_cadastro_basico = {
    'cnpj': [validators.cnpj_validator],
    'razaoSocial': [validators.nome_validator],
}
