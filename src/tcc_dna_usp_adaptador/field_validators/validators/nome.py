from tcc_dna_usp_adaptador.field_validators.common import ValidationResult, field_validator


@field_validator
def nome_validator(_nome: str, **kwargs):
    if type(_nome) != str:
        return ValidationResult.error(value=_nome, field=kwargs.get("field"), reason="Campo com tipo inválido")

    nome = _nome.strip()

    if nome == '':
        return ValidationResult.error(value=nome, field=kwargs.get("field"), reason="Campo vazio")

    return ValidationResult.ok(nome)
