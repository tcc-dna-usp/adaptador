from tcc_dna_usp_adaptador.field_validators.common import ValidationResult, field_validator
from json import load
from re import sub
from pygtrie import Trie

try:
    import importlib.resources as pkg_resources
except ImportError:
    import importlib_resources as pkg_resources

from ... import static

cnae_json = pkg_resources.open_text(static, 'lista_cnae.json')
cnae_set = set()
for word in load(cnae_json):
    for i in range(1, len(word)):
        cnae_set.add(word[:i])


@field_validator
def cnae_validator(_cnae: str, **kwargs):
    cnae = sub(r"[^0-9]", "", str(_cnae)).strip()

    if cnae not in cnae_set:
        return ValidationResult.error(
            value=_cnae,
            field=kwargs.get("field"),
            reason="CNAE inválido"
        )

    return ValidationResult.ok(value=cnae)
