from tcc_dna_usp_adaptador.field_validators.common import ValidationResult, field_validator


@field_validator
def colaboradores_validator(_colaboradores: str, **kwargs):
    try:
        colaboradores = int(_colaboradores)
    except:
        return ValidationResult.error(
            value=_colaboradores,
            field=kwargs.get("field"),
            reason="Qtd de colaboradores não conversível p/ número"
        )

    return ValidationResult.ok(value=colaboradores)
