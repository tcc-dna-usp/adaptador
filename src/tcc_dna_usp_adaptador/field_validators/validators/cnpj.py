from tcc_dna_usp_adaptador.field_validators.common import ValidationResult, field_validator
from validate_docbr import CNPJ
from re import sub

cnpj_v = CNPJ()


@field_validator
def cnpj_validator(_cnpj: str, **kwargs):
    cnpj = sub(r"[^0-9]", "", _cnpj)

    if not cnpj_v.validate(cnpj) and cnpj.upper() != 'EXTERIOR':
        return ValidationResult.error(value=_cnpj, field=kwargs.get("field"), reason="CNPJ Inválido")

    return ValidationResult.ok(cnpj_v.mask(cnpj))
