from tcc_dna_usp_adaptador.field_validators.common import ValidationResult, field_validator
from math import isnan


@field_validator
def faturamento_validator(_faturamento: str, **kwargs):
    faturamento = str(_faturamento).replace(
        'R$', '').replace('.', '').replace(',', '.').strip()

    try:
        faturamento = float(faturamento)
        if isnan(faturamento):
            raise Exception()
    except:
        return ValidationResult.error(
            value=_faturamento,
            field=kwargs.get("field"),
            reason="Faturamento não conversível p/ número"
        )

    return ValidationResult.ok(value=faturamento)
