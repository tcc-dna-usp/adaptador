from pandas import DataFrame
from tcc_dna_usp_adaptador.mapper.presets import *


def map_columns(df: DataFrame, preset=default_column_map_preset, keep_not_mapped_columns=False):
    _df = df.rename(columns=preset)
    return _df if keep_not_mapped_columns else _df[list(dict.values(preset))]
